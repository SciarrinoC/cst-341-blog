package com.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class RegisterController {
	
	

	   @RequestMapping("/register")  
	   public ModelAndView login(HttpServletRequest request,
			   HttpServletResponse response) {
		  String userName=request.getParameter("userName");  
	      String password=request.getParameter("password");
	      String passwordValid = request.getParameter("passwordValid");
		  String firstName=request.getParameter("firstName");
		  String lastName = request.getParameter("lastName");
		  String age = request.getParameter("age");
		  
	      String message;
	      
	      //this will eventually be connected to the database. I simply put my name for the default username and password
	      if(userName != null && 
	    		  !userName.equals("") 
	    		  && userName.equals("Crystal") && 
	    		  password != null && 
	    		  !password.equals("") && 
	    		  password.equals(passwordValid) && 
	    		  firstName != null && 
	    		  firstName != "" && 
	    		  lastName != null && 
	    		  lastName != "" &&
	    		  Integer.parseInt(age) >= 13){
	    	  message = "You have successfully registered! Please Login " +userName + ".";
		      return new ModelAndView("registerSuccess", 
		    		  "message", message);  
	 
	      }
	      
	      else if(firstName == null || firstName =="") {
	    	  message = "You must enter a first name";
	    	  return new ModelAndView("cError", 
	    			  "message", message);
	      }
	      else if(lastName == null || lastName =="") {
	    	  message = "You must enter a last name";
	    	  return new ModelAndView("cError", 
	    			  "message", message);
	      }
	      
	      else if(password == null || password.length()<5) {
	    	  message = "You must enter a password with at least 5 characters";
	    	  return new ModelAndView("cError","message",message);
	      }
	      else if (Integer.parseInt(age) <13) {
	    	  message = "You must be at least 13 years old to use this service";
	    	  return new ModelAndView ("cError", "message", message);
	      }
	      
	      else if (password != passwordValid) {
	    	  message = "Your passwords do not match. Please try again.";
	    	  return new ModelAndView ("cError", "message", message);
	      }
	      
	      
	      else{
	    	  message = "Error.";
	    	  return new ModelAndView("cError", 
	    			  "message", message);
	      }
	   }
	

}

