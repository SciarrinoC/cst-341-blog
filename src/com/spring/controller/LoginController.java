package com.spring.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
public class LoginController {
   @RequestMapping("/login")  
   public ModelAndView login(HttpServletRequest request,
		   HttpServletResponse response) {
	   
	  
	   
	  String userName=request.getParameter("userName");  
      String password=request.getParameter("password");
      String message;
      HttpSession session = request.getSession();
      if(session.getAttribute(userName)!= null && session.getAttribute(password)!=null) {
    	  userName = session.getAttribute("userName").toString();
    	  password = session.getAttribute("password").toString();
      }
      
	
      
      //this will eventually be connected to the database. I simply put my name for the default username and password
      if(userName != null && 
    		  !userName.equals("") 
    		  && userName.equals("Crystal") && 
    		  password != null && 
    		  !password.equals("") && 
    		  password.equals("Sciarrino")){

    	  message = "Welcome " +userName + ".";
    	  
    	  session.setAttribute("username", userName);
    	  session.setAttribute("password", password);
	      return new ModelAndView("welcome", 
	    		  "message", message);  
 
      }else{
    	  message = "Wrong username or password." + session.getAttribute("username").toString() + " "+ session.getAttribute("password").toString();
    	  return new ModelAndView("errorPage", 
    			  "message", message);
      }
   }
}
